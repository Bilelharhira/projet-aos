# Projet jeux AOS
Projet Etudiant AOS, jeux multijoueur sur architecture rest

L'objectif est de faire jouer 2 joueurs.

joueur 1 distribue les cartes 1 a 1 a joueur 2.
Chaque carte a des effets en fonction de si on accepte ou non la carte.

joueur 2 a 4 statistiques VIE, FORCE, MANA, ARGENT.
Son objectif est de maintenir ses 4 statistiques entre 1 et 9. Si il dépasse le 9 ou tombe en dessous de 1 il perd.

La partie prend fin dans 2 situations :
- joueur 1 a joué toutes les cartes du jeux en maintenant ses statistiques a un bon niveau.
- joueur 2 n'a pas réussi a maintenir ses statistiques.

# Démonstration 
http://bilelharhira.yj.fr/login.php

login : bilel
mdp   : harhira



