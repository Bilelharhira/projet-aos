<php
session_start();

$PHPtext = session_id();
?>
<!doctype html>
<html lang="fr">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <title>Projet AOS</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <script src="jquery-3.3.1.min.js"></script>
</head>
<body>
  <div class="py-5 text-center">
    <div class="container">
      <div class="row">
        <div class="col-12" style="">
          <h1 class="display-3">Projet AOS</h1>
          <p class="lead text-muted">Jeux multijoueur sur une architecture REST</p>
          <a href="" onclick="initPartie()"> Réinitialiser la partie </a>
        </div>
      </div>
    </div>
  </div>
	
  <div class="container text-center">
    <div class="row" id="cartes">
    	<div class="col-md-2">
    	</div>

    	<div class="col-md-4 card card-body">
    		<h5 class="card-title"> Joueur 1</h5>
    		<p class="card-text"> Joueur 2 devra vous répondre</p>
    		<a href="MJ.php" class="btn btn-info">commencez !</a>
    	</div>

    	<div class="col-md-1">
    	</div>

    	<div class="col-md-4 card card-body">
    		<h5 class="card-title"> Joueur 2</h5>
    		<p class="card-text"> Vous devrez répondre le joueur 1</p>
			<a href="Joueur.php" class="btn btn-info">commencez !</a>
    	</div>

  </div>
</div>	

<script type="text/javascript">

  var JavaScriptAlert = <php echo json_encode($PHPtext); ?>;
  alert(JavaScriptAlert);
  
  function initPartie() {
    
      var url ="http://bilelharhira.yj.fr/serveur-api-php/INITPARTIE.php";
    
      var xhttp = new XMLHttpRequest();
      
      xhttp.open("POST", url, true);
       xhttp.onreadystatechange = function() {//Call a function when the state changes.
          if(xhttp.readyState == 4 && xhttp.status == 200) {
              alert(xhttp.responseText);
          }
      }
      xhttp.send();
      
      window.location.href="index.php";
    }
</script>


</body>
</html>